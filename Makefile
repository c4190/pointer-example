# Makefile для сборки программы по раздичным методам сортировки.
# Автор: Шмаков И.А. (Igor A. Shmakov)
# Версия: 26.02.2018
# Версия: 12.11.2022
# Лицензия: GPLv3


default: help
.PHONY: default

CXXFLAG = -Wall -pedantic -g -lm -std=c++14
# LINK.o = $(LINK.cc)

sorting: sorting-main.o bubblesort.o minmaxsort.o
	$(LINK.cc) $(CXXFLAG) $^ $(LOADLIBES) $(LDLIBS) -o $@

sorting-v2: sorting-main.v2.o bubblesort.o minmaxsort.o
	$(LINK.cc) $(CXXFLAG) $^ $(LOADLIBES) $(LDLIBS) -o $@

# Help Target
help:
	@echo "Следующие цели представлены в данном Makefile:"
	@echo "... help (Данная цель является целью по умолчанию)"
	@echo "... sorting сборка программы"
.PHONY: help
