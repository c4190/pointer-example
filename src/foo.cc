#include "variable.h"
/*
 * # Автор: Igor A. Shmakov
 * # Первая версия: 2023-02-15
 * # Версия: 0.1
 * # Лицензия: GPLv3
 */

void foo1(int temp) {
  temp = 10;
}

void foo2(int *temp) {
  *temp = 20;
}
