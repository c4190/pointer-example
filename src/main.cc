#include "variable.h"
/*
 * # Автор: Igor A. Shmakov
 * # Первая версия: 2023-02-15
 * # Версия: 0.1
 * # Лицензия: GPLv3
 */

int main(int argc, char *argv[]) {

  // Сортировка методом <<Пузырька>>
  //BubbleSort(sort, unsort, arraysize);

  // Объявление и инициализация переменной
  int val = 5;
  // Объявление и инициализация указателя
  int *ptr = nullptr;

  ptr = &val;

  std::cout << "Значение переменной val: " << val << " до отправки в функции" << std::endl;
  
  // Отправка значения переменной val в функцию
  foo1(val);
  std::cout << "Значение переменной val: " << val << " после foo1" << std::endl;

  // Отправка адреса переменной val в функцию
  foo2(&val);
  std::cout << "Значение переменной val: " << val << " после foo2" << std::endl;

  std::cout << "Значение по указателю *ptr: " << *ptr << std::endl;
  std::cout << "Адрес переменной val: " << &val << std::endl;
  std::cout << "Адрес (значения) указателя ptr: " << ptr << std::endl;  
  
  return 0;
}
