#include <iostream>
#include <ctime>
#include <cstdio>
#include <cstdlib>

/*
 * # Автор: Igor A. Shmakov
 * # Первая версия: 2023-02-15
 * # Версия: 0.1
 * # Лицензия: GPLv3
 */

void foo1(int temp);

void foo2(int *temp);
